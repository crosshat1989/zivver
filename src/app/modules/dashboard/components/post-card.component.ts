import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {
@Input() id: number;
@Input() userId: number;

// Shared state using RxJs can be used in the service as well but this is no good use case here.

displayId$ = new BehaviorSubject<number>(null);
displayId: number;
  constructor() { }

  ngOnInit() {
      this.displayId$.next(this.id);
      this.displayId$.subscribe(data => {
          this.displayId = data;
      });
  }

  onClick() {
      this.displayId === this.id? this.displayId$.next(this.userId): this.displayId$.next(this.id);
  }

}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { PostCardComponent } from './components/post-card.component';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [DashboardComponent, PostCardComponent],
  imports: [
    CommonModule,
    MatCardModule,
    FlexLayoutModule,
    HttpClientModule
  ]
})
export class DashboardModule { }
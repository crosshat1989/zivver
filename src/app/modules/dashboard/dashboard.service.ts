import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    constructor(
        private _http: HttpClient
      ) {}

      getData(): Observable<any> {
          return this._http.get("https://jsonplaceholder.typicode.com/posts").pipe(shareReplay());
      }

}
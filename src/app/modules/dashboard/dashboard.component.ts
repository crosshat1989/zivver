import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    posts: string[];

  constructor(public dashboardService: DashboardService) { }

  ngOnInit(): void {
      this.getAllPosts();
  }

  getAllPosts(): void {
      this.dashboardService.getData().subscribe(data => {
          this.posts = data;
      })
  }

}